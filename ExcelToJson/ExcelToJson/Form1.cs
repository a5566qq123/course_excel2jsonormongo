﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using excel = Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using System.IO;
using MongoDB.Driver;
using System.Web;
using System.Net;
using System.Threading;

namespace ExcelToJson
{
    public partial class ClassExcelToJson : Form
    {
        class Course_Obj
        {
            public string Id { get; set; } //編號
            public string Sem { get; set; } //學年
            public string Course_Id { get; set; } //科目代碼(新碼全碼)
            public string Faculty_Id { get; set; }  //系所代碼
            public string Grad { get; set; } //年級
            public string Course_Name { get; set; } //科目中文名稱
            public string Course_Eng_Name { get; set; } //科目英文名稱
            public string Teacher { get; set; } //老師
            public string Limit_People { get; set; } //限制人數
            public string Course_People { get; set; } //上課人數
            public string Credits { get; set; } //學分
            public string Course_Week { get; set; } //上課週次
            public string Course_Type { get; set; } //課別(必、選修)
            public string Course_Place { get; set; } //上課地點
            public string Course_Day { get; set; } //上課星期
            public string Course_Time { get; set; } //上課節次
            public string Course_Other { get; set; } //課表備註
            public string Faculty_Name { get; set; } //系所
            public string Class_Name { get; set; } //課表名稱(舊碼)
            public string Open_Teacher_Code { get; set;} //開課老師代碼
            public string Open_Teacher { get; set; } //開課老師
            public string Course_Code { get; set; } //科目代碼(舊碼)
            public string Schedule_Code { get; set; } //課表代碼
            public string Class_Plan { get; set; } //教學計劃
        }
        public class Plan_Obj
        {
            public string Course_Name { get; set; }
            public string Class_Name { get; set; }
            public string Open_Teacher { get; set; }
            public string Class_Plan { get; set; }
            public string Course_Code { get; set; }
            public string Schedule_Code { get; set; }
        }


        List<Course_Obj> course_Objs = new List<Course_Obj>();
        List<Course_Obj> dis_course_list = new List<Course_Obj>();
        public ClassExcelToJson()
        {
            InitializeComponent();
        }

        private void Btn_OpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Filter = "Excel file(*.xlsx)|*.xlsx|Old Excel file(*.xls)|*.xls";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = OFD.FileName;
            }
        }

        private void Btn_ReadExcel_Click(object sender, EventArgs e)
        {
            List<Plan_Obj> Plan_lines_split = new List<Plan_Obj>();
            if (!CB_WithoutPlan.Checked)
            {
                string[] Plan_lines = File.ReadAllLines(@textBox2.Text);
                for (int i = 0; i < Plan_lines.Length; i++)
                {
                    string[] splitstr = Plan_lines[i].Split(',');
                    Plan_Obj po = new Plan_Obj();
                    po.Course_Code = splitstr[1].Trim();
                    po.Course_Name = splitstr[2].Trim();
                    po.Class_Name = splitstr[4].Trim();
                    po.Open_Teacher = splitstr[6].Trim();
                    po.Class_Plan = splitstr[splitstr.Length - 1].Trim();
                    string[] Schedule_code_f = po.Class_Plan.Split('-');
                    string Schedule_Code_Result = Schedule_code_f[Schedule_code_f.Length - 1].Split('.')[0];
                    po.Schedule_Code = Schedule_Code_Result;
                    Plan_lines_split.Add(po);
                }
            }
            
            dis_course_list = new List<Course_Obj>();
            excel.Application excelApp = new excel.Application();
            DateTime Stime = DateTime.Now;
            if (excelApp != null)
            {
                excel.Workbook excelWK = excelApp.Workbooks.Open(@textBox1.Text ,0,true);
                excel.Worksheet excelWS = (excel.Worksheet)excelWK.Sheets[1];
                excel.Range Usedrange = excelWS.UsedRange;
                int rowcount = Usedrange.Rows.Count;
                int columncount = Usedrange.Columns.Count +1;
                DataSet ds = new DataSet();
                ds.Tables.Add();
                object[,] excelvalue = Usedrange.Value2;
                for (int i = 1; i< columncount; i++)
                {
                    //excel.Range range = (excelWS.Cells[5,i] as excel.Range);
                    string title = excelvalue[5,i].ToString();
                    ds.Tables[0].Columns.Add(title);
                }
                for (int i = 6; i < rowcount; i++)
                {
                    ds.Tables[0].Rows.Add();
                    for (int j = 1; j < columncount; j++)
                    {
                        //excel.Range range = (excelWS.Cells[i, j] as excel.Range);
                        string cellvalue = excelvalue[i,j].ToString();
                        ds.Tables[0].Rows[i - 6][j - 1] = cellvalue;
                    }
                    Course_Obj _Obj = new Course_Obj();
                    var rows = ds.Tables[0].Rows[i-6];
                    _Obj.Id = rows[0].ToString();
                    _Obj.Course_Day = rows["上課星期"].ToString().Trim();
                    _Obj.Course_Eng_Name = rows["科目英文名稱"].ToString().Trim();
                    _Obj.Course_Id = rows["科目代碼(新碼全碼)"].ToString().Trim();
                    _Obj.Course_Name = rows["科目中文名稱"].ToString().Trim();
                    _Obj.Course_Other = rows["課表備註"].ToString().Trim();
                    _Obj.Course_Place = rows["上課地點"].ToString().Trim();
                    _Obj.Course_Time = rows["上課節次"].ToString().Trim();
                    _Obj.Course_Type = rows["課別名稱"].ToString().Trim();
                    _Obj.Course_Week = rows["上課週次"].ToString().Trim();
                    _Obj.Credits = rows["學分數"].ToString().Trim();
                    _Obj.Faculty_Id = rows["系所代碼"].ToString().Trim();
                    _Obj.Faculty_Name = rows["系所名稱"].ToString().Trim();
                    _Obj.Grad = rows["年級"].ToString().Trim();
                    _Obj.Limit_People = rows["限制人數"].ToString().Trim();
                    _Obj.Sem = rows["學期"].ToString().Trim();
                    _Obj.Teacher = rows["授課教師姓名"].ToString().Trim();
                    _Obj.Open_Teacher = rows["主開課教師姓名"].ToString().Trim();
                    _Obj.Class_Name = rows["課表名稱(舊碼)"].ToString().Trim();
                    _Obj.Course_People = rows["上課人數"].ToString().Trim();
                    _Obj.Course_Code = rows["科目代碼(舊碼)"].ToString().Trim();
                    _Obj.Open_Teacher_Code = rows["主開課教師代碼(舊碼)"].ToString().Trim();
                    _Obj.Schedule_Code = rows["課表代碼(舊碼)"].ToString().Trim();
                    if (!CB_WithoutPlan.Checked)
                    {
                        var test_plan_3 = from data in Plan_lines_split
                                          where
                                        (data.Class_Plan.IndexOf(_Obj.Open_Teacher_Code) >= 0 &&
                                        data.Class_Plan.IndexOf(_Obj.Schedule_Code) >= 0 &&
                                        data.Class_Plan.IndexOf(_Obj.Course_Code) >= 0)
                                          select data;
                        if (test_plan_3.Count() == 1)
                        {
                            _Obj.Class_Plan = test_plan_3.ToList()[0].Class_Plan;
                        }
                        else
                        {
                            var plan_cs = from data in Plan_lines_split
                                          where (data.Class_Plan.IndexOf(_Obj.Open_Teacher_Code) >= 0 &&
                                        data.Class_Plan.IndexOf(_Obj.Course_Code) >= 0) ||
                                        (data.Class_Plan.IndexOf(_Obj.Open_Teacher_Code) >= 0 &&
                                        data.Class_Plan.IndexOf(_Obj.Schedule_Code) >= 0) &&
                                        data.Class_Plan.Split('-').Length < 4
                                          group data by data.Class_Plan into result
                                          select result.First();
                            if (plan_cs.Count() == 1)
                            {
                                _Obj.Class_Plan = plan_cs.ToList()[0].Class_Plan;
                            }
                            else
                            {
                                _Obj.Class_Plan = "無教學計劃";
                            }
                        }
                        course_Objs.Add(_Obj);
                    }
                    _Obj.Class_Plan = "無教學計劃";
                    course_Objs.Add(_Obj);
                }
                
                excelWK.Close();
                excelApp.Quit();
                var courselist = from data in course_Objs group data by data.Id into result select result.First();
                dis_course_list = courselist.ToList();
                dataGridView1.DataSource = dis_course_list;
                //dataGridView1.DataSource = ds.Tables[0];
                course_Objs.Clear();
                DateTime Etime = DateTime.Now;
                TimeSpan pasttime = Etime - Stime;
                MessageBox.Show("讀取完成\r\n花費時間:" + pasttime.TotalSeconds.ToString());
            }

        }

        private void Btn_ToJson_Click(object sender, EventArgs e)
        {
            string json_str = JsonConvert.SerializeObject(dis_course_list);
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Json file(*.json)|*.json";
            if (sfd.ShowDialog()== DialogResult.OK)
            {
                string _filename = sfd.FileName;
                File.WriteAllText(_filename, json_str);
            }
        }

        private void Btn_ToMongo_Click(object sender, EventArgs e)
        {
            // mongodb 連線字串
            var connString = "mongodb://localhost:27017?useNewUrlParser=true&useUnifiedTopology=true";
            //建立 mongo client
            var client = new MongoClient(connString);
            //取得 database
            var db = client.GetDatabase("My_ntunhs");
            //取得 Courses collection
            db.DropCollection("All_Courses");
            var collection = db.GetCollection<Course_Obj>("All_Courses");
            //針對 collection 插入單筆資料
            collection.InsertManyAsync(dis_course_list);
        }


        private void Btn_OpenPlanFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Filter = "txt file(*.txt)|*.txt";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = OFD.FileName;
            }
        }


    }
}
