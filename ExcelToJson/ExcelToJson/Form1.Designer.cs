﻿namespace ExcelToJson
{
    partial class ClassExcelToJson
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_OpenFile = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Btn_ReadExcel = new System.Windows.Forms.Button();
            this.Btn_ToJson = new System.Windows.Forms.Button();
            this.Btn_ToMongo = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.Btn_OpenPlanFile = new System.Windows.Forms.Button();
            this.CB_WithoutPlan = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(70, 17);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(440, 22);
            this.textBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "檔案位置:";
            // 
            // Btn_OpenFile
            // 
            this.Btn_OpenFile.Location = new System.Drawing.Point(516, 16);
            this.Btn_OpenFile.Name = "Btn_OpenFile";
            this.Btn_OpenFile.Size = new System.Drawing.Size(75, 23);
            this.Btn_OpenFile.TabIndex = 2;
            this.Btn_OpenFile.Text = "選擇檔案";
            this.Btn_OpenFile.UseVisualStyleBackColor = true;
            this.Btn_OpenFile.Click += new System.EventHandler(this.Btn_OpenFile_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 87);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(658, 313);
            this.dataGridView1.TabIndex = 3;
            // 
            // Btn_ReadExcel
            // 
            this.Btn_ReadExcel.Location = new System.Drawing.Point(599, 27);
            this.Btn_ReadExcel.Name = "Btn_ReadExcel";
            this.Btn_ReadExcel.Size = new System.Drawing.Size(75, 23);
            this.Btn_ReadExcel.TabIndex = 4;
            this.Btn_ReadExcel.Text = "讀取";
            this.Btn_ReadExcel.UseVisualStyleBackColor = true;
            this.Btn_ReadExcel.Click += new System.EventHandler(this.Btn_ReadExcel_Click);
            // 
            // Btn_ToJson
            // 
            this.Btn_ToJson.Location = new System.Drawing.Point(14, 406);
            this.Btn_ToJson.Name = "Btn_ToJson";
            this.Btn_ToJson.Size = new System.Drawing.Size(114, 23);
            this.Btn_ToJson.TabIndex = 5;
            this.Btn_ToJson.Text = "Export to Json";
            this.Btn_ToJson.UseVisualStyleBackColor = true;
            this.Btn_ToJson.Click += new System.EventHandler(this.Btn_ToJson_Click);
            // 
            // Btn_ToMongo
            // 
            this.Btn_ToMongo.Location = new System.Drawing.Point(154, 403);
            this.Btn_ToMongo.Name = "Btn_ToMongo";
            this.Btn_ToMongo.Size = new System.Drawing.Size(121, 23);
            this.Btn_ToMongo.TabIndex = 6;
            this.Btn_ToMongo.Text = "Export Mongo";
            this.Btn_ToMongo.UseVisualStyleBackColor = true;
            this.Btn_ToMongo.Click += new System.EventHandler(this.Btn_ToMongo_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "教學計劃檔案位置:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(122, 39);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(388, 22);
            this.textBox2.TabIndex = 9;
            // 
            // Btn_OpenPlanFile
            // 
            this.Btn_OpenPlanFile.Location = new System.Drawing.Point(516, 42);
            this.Btn_OpenPlanFile.Name = "Btn_OpenPlanFile";
            this.Btn_OpenPlanFile.Size = new System.Drawing.Size(75, 23);
            this.Btn_OpenPlanFile.TabIndex = 10;
            this.Btn_OpenPlanFile.Text = "選擇檔案";
            this.Btn_OpenPlanFile.UseVisualStyleBackColor = true;
            this.Btn_OpenPlanFile.Click += new System.EventHandler(this.Btn_OpenPlanFile_Click);
            // 
            // CB_WithoutPlan
            // 
            this.CB_WithoutPlan.AutoSize = true;
            this.CB_WithoutPlan.Location = new System.Drawing.Point(12, 65);
            this.CB_WithoutPlan.Name = "CB_WithoutPlan";
            this.CB_WithoutPlan.Size = new System.Drawing.Size(84, 16);
            this.CB_WithoutPlan.TabIndex = 11;
            this.CB_WithoutPlan.Text = "無教學計劃";
            this.CB_WithoutPlan.UseVisualStyleBackColor = true;
            // 
            // ClassExcelToJson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 443);
            this.Controls.Add(this.CB_WithoutPlan);
            this.Controls.Add(this.Btn_OpenPlanFile);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Btn_ToMongo);
            this.Controls.Add(this.Btn_ToJson);
            this.Controls.Add(this.Btn_ReadExcel);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Btn_OpenFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Name = "ClassExcelToJson";
            this.Text = "Class_ExcelToJson";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btn_OpenFile;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button Btn_ReadExcel;
        private System.Windows.Forms.Button Btn_ToJson;
        private System.Windows.Forms.Button Btn_ToMongo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button Btn_OpenPlanFile;
        private System.Windows.Forms.CheckBox CB_WithoutPlan;
    }
}

